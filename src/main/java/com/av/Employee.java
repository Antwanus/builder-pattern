package com.av;

public class Employee {
   private final String firstName; // required
   private final String lastName; // required
   private final int age; // optional
   private final String phone; // optional
   private final String address; // optional

   private Employee(EmployeeBuilder builder) {
      this.firstName = builder.firstName;
      this.lastName = builder.lastName;
      this.age = builder.age;
      this.phone = builder.phone;
      this.address = builder.address;
   }

   // All getter, and NO setter to provide immutability
   public String getFirstName() {return firstName;}
   public String getLastName() {return lastName;}
   public int getAge() {return age;}
   public String getPhone() {return phone;}
   public String getAddress() {return address;}
   @Override
   public String toString() {
      return "Employee{" +
          "firstName='" + firstName + '\'' +
          ", lastName='" + lastName + '\'' +
          ", age=" + age +
          ", phone='" + phone + '\'' +
          ", address='" + address + '\'' +
          '}';
   }

   public static class EmployeeBuilder {
      private final String firstName;
      private final String lastName;
      private int age;
      private String phone;
      private String address;

      public EmployeeBuilder(String firstName, String lastName) {
         this.firstName = firstName;
         this.lastName = lastName;
      }

      public EmployeeBuilder age(int age) {
         this.age = age;
         return this;
      }

      public EmployeeBuilder phone(String phone) {
         this.phone = phone;
         return this;
      }

      public EmployeeBuilder address(String address) {
         this.address = address;
         return this;
      }

      // Return the finally constructed Employee object
      public Employee build() {
         Employee employee = new Employee(this);
         validateEmplopyeeObject(employee);
         return employee;
      }

      private void validateEmplopyeeObject(Employee employee) {
         // Do some basic validations to check
         // if employee object does not break any assumption of system
      }
   }
}
