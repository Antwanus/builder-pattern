package com.av;

public class Main {
   public static void main(String[] args) {
      Employee employee1 = new Employee
          .EmployeeBuilder("Sibin", "Scott") // required
          .age(30)                           // optional
          .phone("9898989898")               // optional
          .address("YYYYYYYYYYY")            // optional
          .build();
      System.out.println(employee1);
   }
}
